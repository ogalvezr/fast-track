# fast-track

Proyecto Symfony paralelo al libro _symfony, la vía rápida_  

# Arrancar el proyecto  

### Arrancar localhost
~~~
symfony server:start -d
~~~
~~~
symfony open:local
~~~

### Arrancar BBDD psql
~~~
docker-compose up -d 
~~~

* Comprobar que está _Up_ y obtener el puerto al que apunta:   
~~~
docker-compose ps 
~~~

* Nos fijamos en el puerto al que está apuntando y lo metemos en el datagrip (click derecho sobre la conexión en el panel izquierdo > Properties), comprobamos conexión  
> PUERTO POR DEFECTO: 32768 -> 5432


#### Si nos da error de que no encuentra los datos migramos de nuevo las entidades: 
~~~
symfony console doctrine:migrations:migrate 
~~~

**OJO!!** esto borrará todos los registros de la base de datos 

* Comprobamos que todo funciona entrando en el panel de admin: 

> https://127.0.0.1:8000/admin/?entity=Conference&action=list&menuIndex=1&submenuIndex=-1